from threading import Thread, Lock

__all__ = [ 'Product' ]

class Product(object):
  '''
  Product detail information

  Attributes:
    __stocks: int - total stocks of product
  '''
  def __init__(self):
    self.__stocksLock = Lock()
    self.__stocks = 0

  def addStock(self, qty):
    with self.__stocksLock:
      self.__stocks += qty

  def order(self, qty):
    ret = False
    with self.__stocksLock:
      if qty <= self.__stocks:
        self.__stocks -= qty
        ret = True

    return ret

if __name__ == '__main__':
  # define new product
  product = Product()
  print('Generate new product')

  # add stock
  qty = 10
  product.addStock(qty)
  print(qty, 'new stocks added')

  # bulk order
  def orderRoutine(customerNumber):
    if not product.order(1):
      print('Customer', customerNumber, 'failed to order product')
    else:
      print('Customer', customerNumber, 'bought a product')

  threads = []
  for i in range(1, 151):
    threads.append(Thread(target=orderRoutine, args=(i,)))
  [ t.start() for t in threads ]
